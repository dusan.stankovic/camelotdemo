import camelot

# create TableList object 'tables' which contains list of Table objects
tables = camelot.read_pdf('futurevaluetables.pdf')

for table in tables:
    # translate tables to DataFrame
    print(table.df)

# output first table to CSV
first_table = tables[0]
first_table.to_csv('first_table.csv')

# output second table to CSV
second_table = tables[1]
second_table.to_csv('second_table.csv')
